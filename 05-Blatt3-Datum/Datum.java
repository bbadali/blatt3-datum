
public class Datum {
	private int tag;
	private int monat;
	private int jahr;
	
	//1-Konstruktor
	Datum()
	{
		tag = 1;
		monat = 1;
		jahr = 1970;
	}
	
	//2-Konstruktor
	Datum(int inTag, int inMonat, int inJahr)
	{
		tag = inTag;
		monat = inMonat;
		jahr = inJahr;
	}
	
	public void setDatum(int aTag, int aMonat, int aJahr)
	{
		tag = aTag;
		monat = aMonat;
		jahr = aJahr;
	}
	
	public int getTag()
	{
		return tag;
	}
	
	public int getMonat()
	{
		return monat;
	}
	
	public int getJahr()
	{
		return jahr;
	}
	
	public int getQuart(int aTag, int aMonat, int aJahr)
	{
		int quart;
		tag = aTag;
		monat = aMonat;
		jahr = aJahr;
		
		if ( 1 <= monat || monat <= 3)
		{
			return 1;
		}
		
		else if ( 4 <= monat || monat <= 6)
		{
			return 2;
		}
		
		else if ( 7 <= monat || monat <= 9)
		{
			return 3;
		}
		
		else if ( 10 <= monat || monat <= 12)
		{
			return 4;
		}
	}

}
